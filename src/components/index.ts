import { App } from "vue";

import { Home, Category, Find, Cart, My } from "@nutui/icons-vue";

import Tabbar from "./tabbar.vue";

const comps = [Tabbar, Home, Category, Find, Cart, My];

export default {
  install(app: App): void {
    comps.forEach((com) => {
      app.component(com.name, com);
    });
  },
};
