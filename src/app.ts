import { createApp } from "vue";
import "./app.scss";

import router from "@/routes";

import comps from "@/components/index";

// import * as echarts from "echarts";

const App = createApp({
  onShow(options) {},
  // 入口组件不需要实现 render 方法，即使实现了也会被 taro 所覆盖
});

// App.component("EChart", EChart);

App.use(router).use(comps);

export default App;
