import { createRouter, createWebHistory } from "vue-router";

import Layout from "@/pages/layout/index.vue";

const routes = [
  {
    path: "/",
    component: Layout,
    children: [
      {
        path: "/",
        name: "index",
        component: () => import("@/pages/index/index.vue"),
      },
      {
        path: "chart1",
        name: "chart1",
        component: () => import("@/pages/chart1/index.vue"),
      },
      {
        path: "user-info",
        name: "user-info",
        component: () => import("@/pages/user-info/index.vue"),
      },
    ],
  },
  {
    path: "/login",
    name: "login",
    component: () => import("@/pages/login/index.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
